#  Equinox: SDR platform for realtime applications
#  Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


###############################################################################
# CMake module for the Google Benchmark suite
# This module defines the variables:
#    BENCHMARK_FOUND: True if the library found, undefined otherwise
#    BENCHMARK_INCLUDE_DIR: The include directory
#    BENCHMARK_LIBRARY: The bechmark library
###############################################################################
FIND_PATH(BENCHMARK_INCLUDE_DIR benchmark/benchmark.h)
FIND_LIBRARY(BENCHMARK_LIBRARY NAMES benchmark)

if (BENCHMARK_INCLUDE_DIR AND BENCHMARK_LIBRARY)
    set(BENCHMARK_FOUND TRUE)
endif (BENCHMARK_INCLUDE_DIR AND BENCHMARK_LIBRARY)

if (BENCHMARK_FOUND)
    if (NOT Benchmark_FIND_QUIETLY)
        message(STATUS "Found Google Benchmark: ${BENCHMARK_LIBRARY}")
    endif (NOT Benchmark_FIND_QUIETLY)
else (BENCHMARK_FOUND)
    if (Benchmark_FIND_REQUIRED)
        message (FATAL_ERROR "Could not find Google Benchmark")
    endif (Benchmark_FIND_REQUIRED)
endif (BENCHMARK_FOUND)