#  Equinox: SDR platform for realtime applications
#  Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

###############################################################################
# Enables memory checking. If this function is not callled at the top-level
# CMakeLists.txt, calls of the add_memtest() have no effect.
###############################################################################
function(eqnx_enable_memcheck )
    set(EQNX_ENABLE_MEMCHECK ON CACHE INTERNAL "EQNX_ENABLE_MEMCHECK")
    find_program(MEMORYCHECK_COMMAND valgrind)
endfunction()

###############################################################################
# Adds a ctest test for memory leakage testing.
# Params:
#    name: The name of the test
#    target: The target to be tested. Target should be a valid target created
#            from add_executable()
###############################################################################
function(add_memtest name target )
    if(EQNX_ENABLE_MEMCHECK)
        list(APPEND memcheck_cmd_options 
            "--trace-children=yes;"
            "--leak-check=full;"
            "--error-exitcode=1;"
        )
        
        add_test(memcheck_${name} 
            ${MEMORYCHECK_COMMAND}
            ${memcheck_cmd_options}
            ${CMAKE_CURRENT_BINARY_DIR}/${target}
        )
    endif()
endfunction()