/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/runtime/kernel.h>
#include <equinox/runtime/runtime_exception.h>

namespace eqnx
{

  kernel::kernel (const std::string& name)
  : d_name (name),
    d_running (false)
  {
  }

  kernel::~kernel () {}

  /**
   *
   * @return the name of the kernel
   */
  const std::string&
  kernel::name () const
  {
    return d_name;
  }

  /**
   * Creates a new input port
   * @param name the name of the input port. This method throws an exception if
   * the kernel is already in the runtime phase or the port name already exists
   */
  void
  kernel::new_input_port (const std::string& name)
  {
    if (d_running) {
      throw std::runtime_error (
          "Port configuration is not allowed during runtime");
    }

    /* Check if there is already a port with the same name */
    if (d_in_ports.find(name) != d_in_ports.end()) {
      throw std::invalid_argument("Port already exists");
    }
    d_in_ports[name] = input_port::make_shared (name);
  }

  /**
   * Creates a new output port
   * @param name the name of the output port. This method throws an exception if
   * the kernel is already in the runtime phase or the port name already exists
   */
  void
  kernel::new_output_port (const std::string& name, size_t msg_num,
                           size_t msg_size)
  {
    if (d_running) {
      throw std::runtime_error (
          "Port configuration is not allowed during runtime");
    }

    /* Check if there is already a port with the same name */
    if (d_out_ports.find(name) != d_out_ports.end()) {
      throw std::invalid_argument("Port already exists");
    }
    d_out_ports[name] = output_port::make_shared (name, msg_num, msg_size);
  }

  /**
   * Get a registered input port
   * @param name the name of the port
   * @return shared pointer of the port
   */
  input_port::sptr
  kernel::input (const std::string& name)
  {
    auto f = d_in_ports.find (name);
    if(f == d_in_ports.end()) {
      throw runtime_exception(runtime_exception::INPUT_PORT_NOT_FOUND);
    }
    return f->second;
  }

  /**
   * Get a registered output port
   * @param name the name of the port
   * @return shared pointer of the port
   */
  output_port::sptr
  kernel::output (const std::string& name)
  {
    auto f = d_out_ports.find (name);
    if(f == d_out_ports.end()) {
      throw runtime_exception(runtime_exception::OUTPUT_PORT_NOT_FOUND);
    }
    return f->second;
  }

  size_t
  kernel::input_num ()
  {
    return d_in_ports.size ();
  }

  size_t
  kernel::output_num ()
  {
    return d_out_ports.size ();
  }

  bool
  kernel::is_source ()
  {
    return d_in_ports.size() == 0;
  }

  bool
  kernel::is_sink ()
  {
    return d_out_ports.size() == 0;
  }

  std::ostream&
  operator<< (std::ostream& out, const kernel& y)
  {
    return out << y.d_name;
  }

  core::connection_anchor
  kernel::operator [] (const std::string& port_name)
  {
    std::shared_ptr<io_port> p;
    try {
      p = input(port_name);
    }
    catch (runtime_exception& e1) {
      try {
        p = output(port_name);
      }
      catch (runtime_exception& e2) {
        throw runtime_exception(runtime_exception::PORT_NOT_FOUND);
      }
    }
    return core::connection_anchor(shared_from_this(), p);
  }

  core::connection_anchor
  kernel::port(const std::string& port_name)
  {
    std::shared_ptr<io_port> p;
    try {
      p = input(port_name);
    }
    catch (runtime_exception& e1) {
      try {
        p = output(port_name);
      }
      catch (runtime_exception& e2) {
        throw runtime_exception(runtime_exception::PORT_NOT_FOUND);
      }
    }
    return core::connection_anchor(shared_from_this(), p);
  }

}  // namespace eqnx

