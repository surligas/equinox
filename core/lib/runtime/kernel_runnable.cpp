/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/runtime/kernel_runnable.h>

namespace eqnx
{

  kernel_runnable::sptr
  kernel_runnable::make_shared(size_t id, size_t worker_id)
  {
    return std::shared_ptr<kernel_runnable>(new kernel_runnable(id, worker_id));
  }

  kernel_runnable::kernel_runnable (size_t id, size_t worker_id)
  : d_id(id),
    d_worker_id(worker_id)
  {
  }

  /**
   *
   * @return the ID of this kernel.
   */
  const size_t
  kernel_runnable::id () const
  {
    return d_id;
  }

  /**
   *
   * @return the ID of the worker that this kernel is assigned to.
   */
  const size_t
  kernel_runnable::worker_id () const
  {
    return d_worker_id;
  }

}  // namespace eqnx
