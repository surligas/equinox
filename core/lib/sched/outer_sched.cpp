/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <thread>
#include <equinox/core/sched/outer_sched.h>

namespace eqnx
{

  namespace core
  {

    /**
     *
     * @return the number of the available cores of the host. If this number
     * cannot be determined this function returns always 1.
     */
    static inline size_t
    get_host_cores_num ()
    {
      size_t ret = std::thread::hardware_concurrency();
      if(ret == 0) {
        return 1;
      }
      return ret;
    }

    outer_sched::outer_sched () :
        outer_sched (get_host_cores_num ())
    {
    }

    outer_sched::outer_sched (size_t n_workers) :
        d_max_n_workers (n_workers),
        d_stop (false),
        d_watchdog_on (false),
        d_watchdog_timeout_secs (1), /* TODO: Get it from config file? */
        d_connections (),
        d_n_workers (0)
    {
    }

    outer_sched::~outer_sched ()
    {
    }

    void
    outer_sched::run ()
    {
      while (!d_stop) {

      }
    }

    void
    outer_sched::stop ()
    {
      d_stop = true;
    }

    void
    outer_sched::enable_watchdog (bool enable)
    {
      d_watchdog_on = enable;
    }

    void
    outer_sched::reset_watchdog ()
    {
    }

    template<typename _Rep>
    void
    outer_sched::watchdog_set_timeout (
          const std::chrono::duration<_Rep>& timeout)
    {
        int64_t t =
            std::chrono::duration_cast<std::chrono::seconds> (timeout).count ();
        if(t > 0) {
          d_watchdog_timeout_secs = t;
        }
        else{
          throw std::runtime_error("Invalid watchdog timeout value");
        }
    }

    void
    operator += (outer_sched& sched, connection::sptr c)
    {
      /* Check if the new connection defines an existing connection */
      for (connection::sptr i : sched.d_connections) {
        if (i->src_kernel () == c->src_kernel ()
            && i->dst_kernel () == c->dst_kernel ()
            && i->src_port () == c->src_port ()
            && i->dst_port () == c->dst_port ()) {
          throw runtime_exception (runtime_exception::CONNECTION_EXISTS_ERROR);
        }
      }
      sched.d_connections.push_back (c);
    }

    void
    outer_sched::_start ()
    {
      d_stop = false;

    }

  }  // namespace core

}  // namespace eqnx

