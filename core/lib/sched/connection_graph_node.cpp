/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/sched/connection_graph_node.h>

namespace eqnx
{

  namespace core
  {

    connection_graph_node::sptr
    connection_graph_node::make_shared ()
    {
      return std::shared_ptr<connection_graph_node> (
          new connection_graph_node ());
    }

    connection_graph_node::connection_graph_node () :
            d_kernel (nullptr),
            d_kernel_set (false),
            d_dfs_visited (false),
            d_depth (-1),
            d_weight (1.0),
            d_worker_id (0)
    {
    }


    /**
     * Set the connection c as input connection
     * @param c input connection to the node
     */
    void
    connection_graph_node::add_input_connection (connection::sptr c)
    {
      if(!d_kernel_set) {
        throw std::runtime_error("Kernel shared pointer is not set");
      }
      d_inputs.push_back(c);
    }

    /**
     * Set the connection c as output connection
     * @param c output connection from this node
     */
    void
    connection_graph_node::add_output_connection (connection::sptr c)
    {
      if(!d_kernel_set) {
        throw std::runtime_error("Kernel shared pointer is not set");
      }
      d_outputs.push_back(c);
    }

    /**
     *
     * @return the weight of this node
     */
    float
    connection_graph_node::weight ()
    {
      return d_weight;
    }

    /**
     * Set the weight of this node
     * @param w a weight value in the range (0,1]. Higher values indicate
     * a more demanding kernel (aka more CPU resources ).
     */
    void
    connection_graph_node::set_weight (float w)
    {
      if (w > 1.0f) {
        throw std::runtime_error ( "Invalid weight on kernel");
      }
      if (w > 0.0) {
        d_weight = w;
        return;
      }
      throw std::runtime_error ( "Invalid weight on kernel");
    }

    /**
     *
     * @return the ID of the worker that this processing kernels is assigned to
     */
    size_t
    connection_graph_node::worker_id ()
    {
      if(!d_kernel_set) {
        throw std::runtime_error("Kernel shared pointer is not set");
      }
      return d_worker_id;
    }

    /**
     * Sets the worker ID that the kernel will operate on
     * @param id the ID of the worker
     */
    void
    connection_graph_node::set_worker_id (size_t id)
    {
      if (!d_kernel_set) {
        throw std::runtime_error ("Kernel shared pointer is not set");
      }
      d_worker_id = id;
    }

    void
    connection_graph_node::set_kernel (kernel::sptr k)
    {
      if(!k) {
        throw std::invalid_argument("Invalid kernel shared pointer");
      }

      /*
       * If the kernel is already set and is the same, just ignore
       * Otherwise raise an error.
       */
      if(d_kernel_set && d_kernel != k) {
        throw std::runtime_error("A different kernel has been already set");
      }
      d_kernel = k;
      d_kernel_set = true;
    }

    /**
     *
     * @return the shared pointer of the kernel associated with this node
     */
    kernel::sptr
    connection_graph_node::get_kernel ()
    {
      if(!d_kernel_set) {
	throw std::runtime_error("Kernel shared pointer is not set");
      }
      return d_kernel;
    }

    void
    connection_graph_node::set_dfs_visited (bool visited)
    {
      d_dfs_visited = visited;
    }

    bool
    connection_graph_node::dfs_visited ()
    {
      return d_dfs_visited;
    }

    /**
     *
     * @return the depth of the current node. If the depth is not yet
     * set -1 is returned
     */
    int
    connection_graph_node::depth ()
    {
      return d_depth;
    }

    /**
     * Set the depth of the node
     * @param d the depth value
     */
    void
    connection_graph_node::set_depth (int d)
    {
      if(d < 0) {
        throw std::runtime_error("Invalid depth value");
      }
      d_depth = d;
    }

    bool
    connection_graph_node::depth_valid ()
    {
      return d_depth > -1 ? true : false;
    }


    /**
     * Resets all the graph analysis related parameters
     */
    void
    connection_graph_node::reset ()
    {
      d_depth = -1;
      for(connection::sptr i : d_inputs) {
        i->set_sorted(false);
      }

      for(connection::sptr i : d_outputs) {
        i->set_sorted(false);
      }
    }

    /**
     *
     * @return the number of input edges for this node
     */
    size_t
    connection_graph_node::get_input_edges_num ()
    {
      return d_inputs.size();
    }

    /**
     *
     * @return the number of output edges for this node
     */
    size_t
    connection_graph_node::get_output_edges_num ()
    {
      return d_outputs.size();
    }

    /**
     * Gets a specific input edge from the graph node
     * @param index the edge index
     * @return a shared pointer of ::connection describing the input edge
     */
    connection::sptr
    connection_graph_node::get_input_connection (size_t index)
    {
      return d_inputs[index];
    }

    /**
     * Gets a specific output edge from the graph node
     * @param index the edge index
     * @return a shared pointer of ::connection describing the output edge
     */
    connection::sptr
    connection_graph_node::get_output_connection (size_t index)
    {
      return d_outputs[index];
    }

    size_t
    connection_graph_node::get_input_edges_num (bool sorted)
    {
      size_t cnt = 0;
      for(connection::sptr i : d_inputs) {
        if(i->is_sorted() == sorted) {
          cnt++;
        }
      }
      return cnt;
    }

  }  // namespace core

}  // namespace eqnx

