/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/sched/inner_sched_rr.h>

namespace eqnx
{

namespace core
{

    inner_sched::sptr
    inner_sched_rr::make_shared ()
    {
      return std::shared_ptr<inner_sched>(new inner_sched_rr());
    }

    inner_sched_rr::inner_sched_rr () :
        inner_sched("inner_sched_rr")
    {
    }

    void
    inner_sched_rr::run (size_t thread_id)
    {
      if(!d_is_ready) {
        throw std::runtime_error("Inner scheduler is not configured");
      }
      while(!d_stop) {
        //TODO
      }
    }

    void
    inner_sched_rr::dry_run (size_t thread_id)
    {
      if(!d_is_ready) {
        throw std::runtime_error("Inner scheduler is not configured");
      }
      while(!d_stop) {
        //TODO
      }
    }

    void
    inner_sched_rr::insert_kernel (kernel_runnable::sptr kernel)
    {
      if(d_is_ready) {
        throw std::runtime_error (
            "Inner scheduler is configured. No more kernels can be assigned");
      }
      d_kernels.push_back(kernel);
    }

}  // namespace core


}  // namespace eqnx
