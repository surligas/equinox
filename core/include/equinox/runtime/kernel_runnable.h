/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_RUNTIME_KERNEL_RUNNABLE_H_
#define CORE_INCLUDE_EQUINOX_RUNTIME_KERNEL_RUNNABLE_H_


#include <equinox/core/core.h>

namespace eqnx
{

  class kernel_runnable
  {
  public:
    typedef std::shared_ptr<kernel_runnable> sptr;

    static sptr
    make_shared(size_t id, size_t worker_id);

    const size_t
    id () const;

    const size_t
    worker_id () const;

  private:
    kernel_runnable (size_t id, size_t worker_id);

    const size_t                d_id;
    const size_t                d_worker_id;
  };

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_RUNTIME_KERNEL_RUNNABLE_H_ */
