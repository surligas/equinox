/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_SCHED_OUTER_SCHED_H_
#define CORE_INCLUDE_EQUINOX_CORE_SCHED_OUTER_SCHED_H_

#include <equinox/core/sched/inner_sched.h>
#include <equinox/core/sched/connection.h>
#include <equinox/runtime/runtime_exception.h>

#include <deque>
#include <vector>
#include <chrono>

namespace eqnx
{

  namespace core
  {

    /**
     * @ingroup sched
     *
     * The outer scheduler is responsible for the creation and management of
     * the worker threads. In each worker thread, a separate inner scheduler is
     * assigned among with the corresponding kernels.
     *
     * During the process termination or after the stop() method invocation
     * the outer scheduler informs the inner schedulers to also stop and exit.
     *
     * In addition, outer scheduler provides a watchdog (by default disabled)
     * that tracks the progress of the worker threads. If no activity has been
     * observed for a specific amount of time, the outer scheduler forces the
     * termination of the worker threads and restarts them. To avoid excess CPU
     * utilization, the necessary progress checks are performed with granularity
     * up to one second. If a more fine-grained watchdog update interval is
     * requested, a runtime error is raised.
     */
    class outer_sched
    {
    public:
      outer_sched ();
      outer_sched (size_t n_workers);

      /* Outer scheduler can NOT be copied */
      outer_sched(const outer_sched& c) = delete;

      ~outer_sched ();

      void
      run ();

      void
      stop ();

      void
      enable_watchdog (bool enable);

      void
      reset_watchdog ();

      template<typename _Rep> void
      watchdog_set_timeout (const std::chrono::duration<_Rep>& timeout);

      friend void
      operator += (outer_sched& sched, connection::sptr c);

    private:
      const size_t                      d_max_n_workers;
      bool                              d_stop;
      bool                              d_watchdog_on;
      uint64_t                          d_watchdog_timeout_secs;
      std::deque<connection::sptr>      d_connections;
      size_t                            d_n_workers;
      std::vector<inner_sched::sptr>    d_workers;

      void
      _start ();

    };

  }  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_SCHED_OUTER_SCHED_H_ */
