/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_SCHED_LOAD_BALANCER_H_
#define CORE_INCLUDE_EQUINOX_CORE_SCHED_LOAD_BALANCER_H_

#include <equinox/core/sched/connection_graph.h>

namespace eqnx
{

  namespace core
  {
    /**
     * @brief Load balancer tries to equalize the processing requirements
     * of the kernels on a graph across the available workers.
     *
     * Depending on the load balancing technique the assignment may be
     * different and it may differently affect the performance. However, there
     * are some requirements that hold for every load balancing technique.
     *
     * The provided number of workers is only a hint to the load balancing
     * subsystem. Depending on the graph analysis and the resulting
     * connected components, the actual number of required workers may be
     * greater (or less in extreme cases).
     * It's upon the runtime subsystem responsibility to decide if such
     * case is valid or not.
     */
    class load_balancer
    {
    public:
      load_balancer (connection_graph::sptr graph, size_t workers_num = 1);

      virtual
      ~load_balancer ();

      size_t
      actual_workers_num ();

      /**
       * Perform graph analysis and then load balancing
       * @return true if the analysis performed without a problem,
       * false other wise
       */
      virtual bool
      analyze () = 0;

      /**
       * Resets the current load balancing analysis and invalidates any
       * kernels assignment
       */
      virtual void
      reset () = 0;

      virtual std::shared_ptr<std::deque<kernel::sptr>>
      get_worker_assignement(size_t index) = 0;

    protected:
      connection_graph::sptr            d_graph;
      const size_t                      d_n_workers;
      size_t                            d_actual_workers;
      bool                              d_graph_analyzed;

    private:
    };
  }  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_SCHED_LOAD_BALANCER_H_ */
