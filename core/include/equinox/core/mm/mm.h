/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_MM_MM_H_
#define CORE_INCLUDE_EQUINOX_MM_MM_H_

#include <cstdlib>

namespace eqnx
{
  /**
   * @defgroup core
   */
  namespace core
  {

    /**
     * @ingroup core
     */
    class mm
    {
    public:
      mm(size_t nthreads);
      ~mm();
    private:
    };

  }  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_MM_MM_H_ */
