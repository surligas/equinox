/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_LOG_H_
#define CORE_INCLUDE_EQUINOX_LOG_H_

#include "config.h"

#include <cstdio>
#include <ctime>

#define EQNX_LOG_INFO(M, ...)                                           \
    do{                                                                 \
      time_t secs = time(0);                                            \
      struct tm local;                                                  \
      localtime_r(&secs, &local);                                       \
      fprintf(stderr, "[%02d:%02d:%02d][INFO]: " M " \n",               \
              local.tm_hour, local.tm_min, local.tm_sec, ##__VA_ARGS__);\
    }while(0)

#define EQNX_LOG_ERROR(M, ...)                                          \
    do{                                                                 \
      time_t secs = time(0);                                            \
      struct tm local;                                                  \
      localtime_r(&secs, &local);                                       \
      fprintf(stderr, "[%02d:%02d:%02d][ERROR] %s:%d: " M "\n",         \
                local.tm_hour, local.tm_min, local.tm_sec,              \
                __FILE__, __LINE__,                                     \
                ##__VA_ARGS__);                                         \
    }while(0)

#define EQNX_LOG_WARN(M, ...)                                           \
    do{                                                                 \
      time_t secs = time(0);                                            \
      struct tm local;                                                  \
      localtime_r(&secs, &local);                                       \
      fprintf(stderr, "[%02d:%02d:%02d][WARN] %s:%d: " M "\n",          \
                local.tm_hour, local.tm_min, local.tm_sec,              \
                __FILE__, __LINE__,                                     \
                ##__VA_ARGS__);                                         \
    }while(0)

#if EQNX_DEBUG
#define EQNX_LOG_DEBUG(M, ...)                                         \
    do{                                                                 \
      time_t secs = time(0);                                            \
      struct tm local;                                                  \
      localtime_r(&secs, &local);                                       \
      fprintf(stderr, "[%02d:%02d:%02d][DEBUG]: " M " \n",              \
              local.tm_hour, local.tm_min, local.tm_sec, ##__VA_ARGS__);\
    }while(0)
#else
define EQNX_LOG_DEBUG(M, ...)
#endif

#endif /* CORE_INCLUDE_EQUINOX_LOG_H_ */
