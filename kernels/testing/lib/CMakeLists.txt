#
#  Equinox: SDR platform for realtime applications
#  Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

list(APPEND kernels_testing_sources
    in_out.cpp
    sink.cpp
    source.cpp
)

add_library(equinox-kernels-testing SHARED ${kernels_testing_sources})

target_include_directories(equinox-kernels-testing
    PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_CURRENT_BINARY_DIR}
    ${EQNX_KERNELS_TESTING_INCLUDE_DIRS}
    ${EQNX_CORE_INCLUDE_DIRS}
)

target_link_libraries(equinox-kernels-testing equinox-core)
