/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/kernels/testing/sink.h>

namespace eqnx
{

  namespace testing
  {

    sink::sptr
    sink::make_shared (const std::string& name, size_t nports)
    {
      return std::shared_ptr<sink> (new sink (name, nports));
    }

    eqnx::testing::sink::sink (const std::string& name, size_t nports) :
            kernel (name)
    {
      for (size_t i = 0; i < nports; i++) {
        const std::string pname ("in" + std::to_string (i));
        new_input_port (pname);
      }
    }

  }  // namespace testing

}  // namespace eqnx

