#
#  Equinox: SDR platform for realtime applications
#  Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.0)
project(equinox CXX C)


# Equinox is a C++11 targeted platform
set (CMAKE_CXX_STANDARD 11)
add_definitions(-std=c++11)

###############################################################################
# CMake custom modules
###############################################################################
list(INSERT CMAKE_MODULE_PATH 0 ${CMAKE_SOURCE_DIR}/cmake)

# uninstall target
configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/cmake/cmake_uninstall.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/cmake/cmake_uninstall.cmake"
    IMMEDIATE @ONLY)

add_custom_target(uninstall
    COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake/cmake_uninstall.cmake)

# Set the version information here
set(VERSION_INFO_MAJOR_VERSION 0)
set(VERSION_INFO_API_COMPAT    0)
set(VERSION_INFO_MINOR_VERSION 1)
set(VERSION_INFO_MAINT_VERSION git)

# Set the versioning. 
include(EqnxVersion)


###############################################################################
# Equinox build external dependencies    
###############################################################################
# Mandatory requirements
find_package(Threads REQUIRED)
find_package(Config++ REQUIRED)

# Optional requirements. Dependent components are automatically removed from
# the build tree
find_package(Doxygen)
find_package(CppUnit)
find_package(Benchmark)
find_package(Numa)

# Handle properly all the missing optional packages
if(NOT NUMA_FOUND)
    message(WARNING "NUMA library not found. Continue without NUMA support." 
    " This may affect performance.")
endif()

# Eigen3 for graph partitioning
find_package(Eigen3)

###############################################################################
# Equinox user defined options
###############################################################################
option(ENABLE_DOXYGEN_DOCS "Enable Doxygen Documentation" OFF)

option(ENABLE_TESTING "Enable Unit testing" ON)

option(ENABLE_MEMCHECK 
    "Enable a full memory check on the Unit testing (valgrind tool required)" ON
)

option(ENABLE_BENCHMARK 
    "Enable benchmarking of Equinox subsystems (requires googlebench suite)" ON
)


###############################################################################
# Equinox testing
###############################################################################
if(ENABLE_TESTING AND CPPUNIT_FOUND)
    set(EQNX_ENABLE_TESTING ON)
    include(EqnxTesting)
    # Enable memcheck on the unit tests
    if(ENABLE_MEMCHECK)
        eqnx_enable_memcheck()
    endif()
    include (CTest)
    enable_testing()
else()
    set(EQNX_ENABLE_TESTING OFF)
    set(EQNX_ENABLE_MEMCHECK OFF)
endif()

###############################################################################
# Equinox performance benchmarks
###############################################################################
if(ENABLE_BENCHMARK AND BENCHMARK_FOUND)
    # CTest may have not be included if Unit tests are disabled
    include (CTest)
    enable_testing()
    set(EQNX_ENABLE_BENCHMARK ON)
else()
    set(EQNX_ENABLE_BENCHMARK OFF)
endif()

###############################################################################
# Installation details                
###############################################################################
include(CPack)
include(EqnxLib)
include(EqnxSummary)

set(EQNX_RUNTIME_DIR     bin CACHE PATH "Path to install all binaries")
set(EQNX_LIBRARY_DIR     lib${LIB_SUFFIX} CACHE PATH "Path to install libraries")
set(EQNX_INCLUDE_DIR     include CACHE PATH "Path to install header files")
set(EQNX_DATA_DIR        share CACHE PATH "Base location for data")

# Mark all the advance variables
mark_as_advanced(EQNX_DATA_DIR)

include(EqnxUtils)

###############################################################################
# Include all the submodules
###############################################################################
add_subdirectory(core)
add_subdirectory(docs)
add_subdirectory(kernels)

###############################################################################
# Print configuration summary
###############################################################################
EQNX_PRINT_SUMMARY()
